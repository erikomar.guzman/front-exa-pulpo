
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Vehiculo } from '../../model/transporte.model';
import { TransporteService } from '../../service/transporte.service';
import { CatalogosService } from '../../service/catalogos.service';
import { Color, Marca } from '../../model/catalogos.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DialogInputComponent } from '../../shared/dialog-input/dialog-input.component';
import { MatDialog } from '@angular/material/dialog';
import { AuteticacionService } from '../../service/auteticacion.service';
import { Credenciales } from '../../model/usuarios.model';

@Component({
  selector: 'front-plp-transp-vehiculos',
  templateUrl: './vehiculos.component.html',
  styleUrls: ['./vehiculos.component.scss']
})
export class VehiculosComponent implements OnInit {
  @ViewChild('paginatorvehiculos') paginatorvehiculos?: MatPaginator ;

  constructor(
    private transporteService: TransporteService,
    private catalogosService: CatalogosService,
    private auteticacionService: AuteticacionService,
    private fb: FormBuilder,
    private dialogRef: MatDialog,
  ) { }
  procesandoDatos = false;
  lstColores: Color[] = [];
  lstMarcas: Marca[] = [];
  lstvehiculos = new MatTableDataSource<Vehiculo>();
  vehiculoAct?: Vehiculo;
  tituloColumnas = ['id','identificacion','marca', 'modelo','color','fechaIngreso','estado','asignado','opciones'];
  vehiculoForm?: FormGroup;
  verFormulario = false;
  esEditar = false;
  asignado = false;
  activo = false;
  credenciales?: Credenciales;
  ngOnInit(): void {
    this.getCredenciales();
    this.actualizaFormulario();
    this.obtenerVhiculos();
    this.obtenerCatalogos();
  }
  getCredenciales(){
    this.credenciales= this.auteticacionService.getCredenciales();
  }
  obtenerVhiculos(): void{
    this.transporteService.obtenerVehiculos().subscribe(
      (response) =>{
          this.lstvehiculos = new MatTableDataSource<Vehiculo>(response);
          if (this.paginatorvehiculos != null)
            this.lstvehiculos.paginator = this.paginatorvehiculos;
      },
      (error) => {
        console.log(error);
        this.dlgMensaje('Transporte', 'No es posible recuperar los vehiculos');
      }
    );
  }
  agregarVehiculo(nvo: Vehiculo): void{
    this.transporteService.agregarVehiculo(nvo).subscribe(
      (response) => {
        this.dlgMensaje('Vehiculos', 'Se agrego corectamete el nuevo vehículo');
        this.obtenerVhiculos();
        this.cancelar();
      },
      (error) => {
        this.dlgMensaje('Vehiculos', 'Ocurrio un error, no se agrego el vehículo');
      }
    );
  }
  actualizarVehiculo(vehiculo: Vehiculo): void{
    this.transporteService.agregarVehiculo(vehiculo).subscribe(
      (response) => {
        this.dlgMensaje('Vehiculos', 'Se actualizó corectamete el nuevo vehículo');
        this.obtenerVhiculos();
        this.cancelar();
      },
      (error) => {
        this.dlgMensaje('Vehiculos', 'Ocurrio un error, no se actalizó el vehículo');

      }
    );
  }
  obtenerCatalogos():void{
    this.catalogosService.obtenerColores().subscribe(
      (response)=>{
        this.lstColores = response;
      },
      (error) => {
        console.log(error);
        this.dlgMensaje('Transporte', 'No es posible recuperar los el catálogo de colores');
      }
    );
    this.catalogosService.obtenerMarca().subscribe(
      (response2)=>{
        this.lstMarcas = response2;
      },
      (error) => {
        console.log(error);
        this.dlgMensaje('Transporte', 'No es posible recuperar el catálogo de marcas');
      }
    );
  }

  editarVehiculo(row: Vehiculo): void {
    this.vehiculoAct = row;
    this.actualizaFormulario()
    this.esEditar = true;
    this.verFormulario=true;
  }
  verNovedades(row: any): void {}

  actualizaFormulario(){
    this.vehiculoForm = this.fb.group({
      identificacion:[this.vehiculoAct?.identificacion, Validators.required],
      marca:[this.vehiculoAct?.marca?.marca, Validators.required],
      modelo:[this.vehiculoAct?.modelo, Validators.required],
      color:[this.vehiculoAct?.color?.color, Validators.required],
      activo:[this.vehiculoAct?.activo, Validators.required],
      asignado:[this.vehiculoAct?.asignado, Validators.required],
    });
  }
  borrar(row: Vehiculo):void{
    if(row?.id)
      this.transporteService.borrarVehiculo(row.id).subscribe(
        (response) => {
          if(response.isOk){
            this.dlgMensaje('Vehiculos', 'Se eliminó corectamete el vehículo');
            this.obtenerVhiculos();
            
          }
          else  
            this.dlgMensaje('Vehiculos', 'No se eliminó el nuevo vehículo');
        },
        (error) => {
          this.dlgMensaje('Vehiculos', 'Ocurrio un error, no se eliminó el vehículo');
        }
      );
  }
  agregar():void{
    this.vehiculoAct = {
      activo: false,
      asignado:false,
    };
    this.actualizaFormulario()
    this.esEditar = false;
    this.verFormulario=true;
  }
  cancelar(){
    this.vehiculoAct = {
      activo: false,
      asignado:false,
    };
    this.actualizaFormulario()
    this.esEditar = false;
    this.verFormulario=false;
  }
  cambioAsignado(event: any){
    if(this.vehiculoAct)
      this.vehiculoAct.asignado = event.checked;
  }
  cambioActivo(event: any){
    if(this.vehiculoAct)
      this.vehiculoAct.activo = event.checked;
    
  }
  enviarDatos():void{
    
    const vehiculo: Vehiculo = {
      id: this.vehiculoAct?.id,
      identificacion: this.vehiculoForm?.get('identificacion')?.value,
      modelo: this.vehiculoForm?.get('modelo')?.value,
      activo: this.vehiculoAct?.activo,
      asignado: this.vehiculoAct?.asignado,
      color: {color: this.vehiculoForm?.get('color')?.value},
      marca: {marca: this.vehiculoForm?.get('marca')?.value},
    }
    if(this.esEditar)
      this.actualizarVehiculo(vehiculo);
    else
      this.agregarVehiculo(vehiculo);
  }
  dlgMensaje(tituloMs:string , mensajeMs: string){
    const diaologo = this.dialogRef.open(DialogInputComponent,{
      width: '350px',
      data:{
        titulo:tituloMs,
        mensaje: mensajeMs,
        isOkcancel: false,
      }
    });
  }
}
