import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AuteticacionService } from '../../service/auteticacion.service';

@Component({
  selector: 'front-plp-transp-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private autenticacionService: AuteticacionService,
    ) {}

  loginForm = this.fb.group({
    usuario: [null, Validators.required],
    password: [null, Validators.required]
  });

  hayError = false;
  errorMessage = 'Invalid Credentials';
  successMessage: string = '';
  invalidLogin = false;
  loginSuccess = false;
  hide = true;
  

  ngOnInit(): void {
  }

  onSubmit() {
    this.autenticacionService.validaUsuario(
      this.loginForm.get('usuario')?.value,
      this.loginForm.get('password')?.value
    ).subscribe(
      (response) => {
        this.hayError=false;
        this.autenticacionService.registraCredenciales(response);
        console.log(this.autenticacionService.getCredenciales());
        this.router.navigate(['/vehiculos']);
      },
      (error) => {
        this.hayError= true;
        console.log(error);
      }
    );
  }
}
