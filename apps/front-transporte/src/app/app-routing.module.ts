import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginComponent } from "./components/login/login.component";
import { VehiculosComponent } from "./components/vehiculos/vehiculos.component";
import { AuthGuard } from "./service/auth.guard";

const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'vehiculos', component: VehiculosComponent,canActivate: [AuthGuard] },
    { path: '', pathMatch: 'full', redirectTo: '/login' },
  ];
  
  @NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
  })
  export class AppRoutingModule {}
  