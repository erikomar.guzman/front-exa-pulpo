import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstants } from '../config/appConstants';
import { Observable } from 'rxjs';
import { Credenciales } from '../model/usuarios.model';

@Injectable({
  providedIn: 'root'
})
export class AuteticacionService {
  
  constructor(
    private httpClient: HttpClient,
    private appConstants: AppConstants
  ) { }

  public validaUsuario(usuario: string, password: string): Observable<Credenciales>{
    return this.httpClient.post<Credenciales>(
      this.appConstants.URL_USUARIOS_LOGIN, {usuario:usuario, password:password}
    )
  }
  public registraCredenciales(cred: Credenciales){
    sessionStorage.setItem('credenciales', JSON.stringify(cred));
  }
  public getCredenciales(): Credenciales{
    const cred = sessionStorage.getItem('credenciales');
    if (cred)
      return JSON.parse(cred);
    else {
      return {
        token:''
      };
    };
  }
}
