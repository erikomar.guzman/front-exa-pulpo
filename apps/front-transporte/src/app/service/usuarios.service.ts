import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppConstants } from '../config/appConstants';
import { Usuario } from '../model/usuarios.model';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(
    private httpClient: HttpClient,
    private appConstants: AppConstants
  ) { }

  validaUsuario(user: string, pass: string): Observable<Usuario> {
    return this.httpClient.post<Usuario>(
      this.appConstants.URL_USUARIOS_LOGIN, {usuario:user, password: pass}
    );
  }
}
