import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstants } from '../config/appConstants';
import { Observable } from 'rxjs';
import { Color, Marca } from '../model/catalogos.model';

@Injectable({
  providedIn: 'root'
})
export class CatalogosService {

  constructor(
    private httpclient: HttpClient,
    private appConstants: AppConstants
  ) { }

  public obtenerColores(): Observable<Color[]>{
    return this.httpclient.get<Color[]>(
      this.appConstants.URL_CATALOGOS +'/colores'
    );
  }
  public obtenerColor(id: number): Observable<Color>{
    return this.httpclient.get<Color>(
      this.appConstants.URL_CATALOGOS +'/colores/'+ id
    ); 
  }
  public agregarColor(nvo: Color): Observable<Color>{
    return this.httpclient.post<Color>(
      this.appConstants.URL_CATALOGOS +'/colores', nvo
    );
  }
  public actualizarColor(nvo: Color): Observable<Color>{
    return this.httpclient.put<Color>(
      this.appConstants.URL_CATALOGOS +'/colores', nvo
    );
  }


  // NOVEDADES
  public obtenerMarca(): Observable<Marca[]>{
    return this.httpclient.get<Marca[]>(
      this.appConstants.URL_CATALOGOS +'/marcas'
    );
  }
  public obtenerNovedad(id: number): Observable<Marca>{
    return this.httpclient.get<Marca>(
      this.appConstants.URL_CATALOGOS +'/marcas/'+ id
    ); 
  }
  public agregarMarca(nvo: Marca): Observable<Marca>{
    return this.httpclient.post<Marca>(
      this.appConstants.URL_CATALOGOS +'/marcas', nvo
    );
  }
  public actualizarMarca(nvo: Marca): Observable<Marca>{
    return this.httpclient.put<Marca>(
      this.appConstants.URL_CATALOGOS +'/marcas', nvo
    );
  }
}