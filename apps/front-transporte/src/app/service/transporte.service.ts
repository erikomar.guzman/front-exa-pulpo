import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstants } from '../config/appConstants';
import { Novedades, Vehiculo } from '../model/transporte.model';
import { Observable } from 'rxjs';
import { RespGenerica } from '../model/util.model';

@Injectable({
  providedIn: 'root'
})
export class TransporteService {

  constructor(
    private httpclient: HttpClient,
    private appConstants: AppConstants
  ) { }

  public obtenerVehiculos(): Observable<Vehiculo[]>{
    return this.httpclient.get<Vehiculo[]>(
      this.appConstants.URL_TRANSPORTE +'/vehiculos'
    );
  }
  public obtenerVehiculo(id: number): Observable<Vehiculo>{
    return this.httpclient.get<Vehiculo>(
      this.appConstants.URL_TRANSPORTE +'/vehiculos/'+ id
    ); 
  }
  public agregarVehiculo(nvo: Vehiculo): Observable<Vehiculo>{
    return this.httpclient.post<Vehiculo>(
      this.appConstants.URL_TRANSPORTE +'/vehiculos', nvo
    );
  }
  public actualizarVehiculo(nvo: Vehiculo): Observable<Vehiculo>{
    return this.httpclient.put<Vehiculo>(
      this.appConstants.URL_TRANSPORTE +'/vehiculos', nvo
    );
  }
  public borrarVehiculo(id: number): Observable<RespGenerica>{
    return this.httpclient.delete<RespGenerica>(
      this.appConstants.URL_TRANSPORTE +'/vehiculos/'+ id
    );
  }

  // NOVEDADES
  public obtenerNovedades(): Observable<Novedades[]>{
    return this.httpclient.get<Novedades[]>(
      this.appConstants.URL_TRANSPORTE +'/novedades'
    );
  }
  public obtenerNovedad(id: number): Observable<Vehiculo>{
    return this.httpclient.get<Novedades>(
      this.appConstants.URL_TRANSPORTE +'/novedades/'+ id
    ); 
  }
  public agregarNovedades(nvo: Novedades): Observable<Novedades>{
    return this.httpclient.post<Novedades>(
      this.appConstants.URL_TRANSPORTE +'/novedades', nvo
    );
  }
  public actualizarNovedades(nvo: Novedades): Observable<Novedades>{
    return this.httpclient.put<Novedades>(
      this.appConstants.URL_TRANSPORTE +'/novedades', nvo
    );
  }
}
