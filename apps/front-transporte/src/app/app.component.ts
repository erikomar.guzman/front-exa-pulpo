import { Component, OnChanges } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UsuariosService } from './service/usuarios.service';
import { AuteticacionService } from './service/auteticacion.service';
import { Credenciales } from './model/usuarios.model';

@Component({
  selector: 'front-plp-transp-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnChanges {
  title = 'EXAMEN DE GUFE';
  usuario = '';
  
  constructor(
    private autenticacionService: AuteticacionService,
  ){}
  
  ngOnChanges(){
    this.leerCredenciales();
  }

  leerCredenciales(){
    const credenciales = this.autenticacionService.getCredenciales();
    if(credenciales.usuario)
      this.usuario = credenciales.usuario.nombre;
    else
      this.usuario='';
  }
}
