import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData, DialogRespData } from '../../model/util.model';

@Component({
  selector: 'front-plp-transp-dialog-input',
  templateUrl: './dialog-input.component.html',
  styleUrls: ['./dialog-input.component.scss']
})
export class DialogInputComponent implements OnInit {

  constructor(
    public dialogre: MatDialogRef<DialogInputComponent>,
    @Inject(MAT_DIALOG_DATA) public datos:DialogData
  ) { }

  ngOnInit(): void {
  }

  onOkClick():void {
    let resp = this.valorReturn();
    this.dialogre.close(resp);
  }
  onCancelclick(): void {
    this.dialogre.close();
  }
  valorReturn():DialogRespData {
    let resp: DialogRespData = {
      inTextVal: this.datos.inTextVal,
      inOpcionSel: this.datos.inOpcionSel
    }
    return resp;
  }
}
