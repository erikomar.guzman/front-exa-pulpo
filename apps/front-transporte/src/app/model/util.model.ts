export interface DialogData{
    titulo: string,
    mensaje?: string,
    inTextLabel?: string,
    inTextVal?: string,
    inTextShow?: boolean,
    inOpcionLabel?: string,
    inOpcionValrs?: opcion [],  
    inOpcionShow?: boolean,
    inOpcionSel?: opcion,
    btnOkLabel?: string,   
    isOkcancel: boolean    
}
export interface DialogRespData{
    inTextVal?: string,
    inOpcionSel?: opcion
}
export interface opcion{
    cadena: string,
    valor?: number,
    seleccionado?: boolean,
    visible?: boolean
}
export interface RespGenerica{
    isOk?: boolean;
    mensaje?: string;
}