
export interface Usuario {
    id: number;
    nombre: string;
    usuario: string;
    rol?: Rol;
}
export interface Rol {
    rol: number;
    descripcion: string;
}

export interface Credenciales{
    token?: string;
    usuario?: Usuario;
}