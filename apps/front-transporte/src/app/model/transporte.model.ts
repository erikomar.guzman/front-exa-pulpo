import { Color, Marca } from "./catalogos.model";

export interface Vehiculo{
    id?: number;
    identificacion?: string;
    modelo?: number;
    fechaIngreso?: Date;
    activo?: boolean;
    asignado?: boolean;
    color?: Color;
    marca?: Marca;
}

export interface Novedades{
    id?: number;
    descripcion?: string;
    fecha?: Date;
    vehiculo?:Vehiculo;
}