import { Injectable } from "@angular/core";

@Injectable()
export class AppConstants {
    readonly URL_SERVER ='http://localhost:3000';

    readonly URL_USUARIOS = this.URL_SERVER + '/usuarios';
    readonly URL_USUARIOS_LOGIN = this.URL_SERVER + '/auth/login';
    readonly URL_TRANSPORTE = this.URL_SERVER + '/transporte';
    readonly URL_CATALOGOS = this.URL_SERVER + '/catalogos';
}