export const getGreeting = () => cy.get('h1');
export const getFrontTransporte = () => cy.get('li.todo');
export const getAddFrontButton = () => cy.get('button#add-todo');
